//Projeto Pisca Led

void setup(){
    pinMode(9,OUTPUT);//Aqui esta a declaraçao do pino e sua funçao
    pinMode(10,OUTPUT);
    pinMode(11,OUTPUT);
}

void loop () {
  //controle do led verde
  digitalWrite(9,HIGH); //aqui ele manda o valor maximo (energia =1)
  delay(4000);//para aguardar 1 segundo
  digitalWrite(9,LOW);//aqui ele tira a energia (energia=0)
  
  //controle do led amarelo
  digitalWrite(10,HIGH);
  delay(2000);
  digitalWrite(10,LOW);
 
  //controle led vermelho
  digitalWrite(11,HIGH);
  delay(4000);
  digitalWrite(11,LOW); 
}//loop roda tudo sem parar
