int led = D0;
int luminosidade = 0;

void setup() {
  // initialize digital pin D0 as an output.
  pinMode(A0, INPUT);
  pinMode(D0, OUTPUT);

  Serial.begin(115200);
  Serial.println("Iniciando NodeMCU..........................");
}

// the loop function runs over and over again forever
void loop() {
  
  luminosidade = analogRead(A0);
  int pwm = 255 - (luminosidade / 4);
  analogWrite(led, pwm); //controlando dinamicamente o led
  
  Serial.print("Luz:  "); //mensagem
  Serial.print(luminosidade); //variavel
  Serial.print("    - pwm");
  Serial.print(pwm);
  Serial.println();//quebra de linha
  
  delay(1000);
}
