int farol = 13, farol2 = 12;

void setup() {                
  pinMode(farol, OUTPUT);
  pinMode(farol2, OUTPUT);  
}

void loop() {
  digitalWrite(farol, HIGH);
  digitalWrite(farol2, HIGH);
  delay(3000);               
  digitalWrite(farol, LOW);   
  digitalWrite(farol2, LOW);
  delay(1000);              
}
