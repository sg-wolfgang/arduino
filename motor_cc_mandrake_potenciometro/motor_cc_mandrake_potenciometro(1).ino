int const potenciometro = 0; //pino analógico onde o potenciômetro está conectado.
int const transistor = 9;    //pino digital onde o TIP120 está conectado.
int valPotenciometro = 0;    //armazenará o valor lido no potenciômetro.

//função setup é executada uma vez quando o arduino é ligado.
void setup() {
  pinMode(transistor, OUTPUT); //definindo o pino digital 9 como de saída.
}

//Função loop é executada repetidamente enquanto o arduino estiver ligado.
void loop() {    
  //lendo o valor do potenciômetro (0 até 1023) e dividindo por 4 para obter um
  //valor entre 0 e 255 que são valores adequados para a base do TIP120.
  valPotenciometro = analogRead(potenciometro) / 4;
 
  //atribuindo o valor lido no potenciômetro a base do TIP120.  
  analogWrite(transistor, valPotenciometro);
} 
