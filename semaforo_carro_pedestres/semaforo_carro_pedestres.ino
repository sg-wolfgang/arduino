int led1 = D0, led2 = D1, led3 = D2,  led4 = D3, led5 = D4;

void setup() {
    pinMode(led1,OUTPUT);//Led Verde//para pinos digitais, uso pinMode
    pinMode(led2,OUTPUT);//Led Amarelo
    pinMode(led3,OUTPUT);//Led Vermelho
    pinMode(led4,OUTPUT);//Led Verde Pedestre
    pinMode(led5,OUTPUT);//Led Vermelho Pedestre
    Serial.begin(115200);
}

void loop() {
  digitalWrite(led1,HIGH);
  digitalWrite(led5,HIGH);
  Serial.println("CARROS podem passar, PEDESTRES devem esperar!");
  delay(4000);
  
  digitalWrite(led1,LOW);
  digitalWrite(led2,HIGH);
  Serial.println("ATENÇÃO - CARROS E PEDESTRES!");
  delay(1500);
  
  digitalWrite(led2,LOW);
  digitalWrite(led5,LOW);
  digitalWrite(led3,HIGH);
  digitalWrite(led4,HIGH);
  Serial.println("PEDESTRES podem passar, CARROS devem esperar!");
  delay(4000);
  
  digitalWrite(led3,LOW);
  digitalWrite(led4,LOW);
    
}
