int led1 = 7, led2 = 8, led3 = 9,  led4 = 2, led5 = 3;

void setup() {
 pinMode(led1,OUTPUT);//para pinos digitais, uso pinMode
 pinMode(led2,OUTPUT);
 pinMode(led3,OUTPUT);
 pinMode(led4,OUTPUT);
 pinMode(led5,OUTPUT);
}

void loop() {
  digitalWrite(led1,HIGH);
  digitalWrite(led5,HIGH);
  delay(4000);
  digitalWrite(led1,LOW);
  digitalWrite(led2,HIGH);
  delay(1500);
  digitalWrite(led2,LOW);
  digitalWrite(led5,LOW);
  digitalWrite(led3,HIGH);
  digitalWrite(led4,HIGH);
  delay(4000);
  digitalWrite(led3,LOW);
  digitalWrite(led4,LOW);  
}
