/*
  Projeto Acender Led com Potenciometro
 */
int led = 8;
void setup() {
  // initialize serial communication at 9600 bits per second:
  pinMode (led, OUTPUT);
  
  Serial.begin(9600);
}

void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  if (sensorValue>0){
  analogWrite(led, (sensorValue/4));
  Serial.println(sensorValue);}
}
