//Projeto: Medir temperatura e acionar lampada quando ficar escuro
//Autor: Samuel Goncalves Pereira
//Este e um software livre, Free for all the peoples!!
//XWorkshopUEG2015

// Pino onde o Termistor esta conectado
#define PINOTERMISTOR A0         
// Valor do termistor na temperatura nominal
#define TERMISTORNOMINAL 10000      
// Temp. nominal descrita no Manual
#define TEMPERATURENOMINAL 25   
// Número de amostragens para 
#define NUMAMOSTRAS 5
// Beta do nosso Termistor
#define BCOEFFICIENT 3977
// valor do resistor em série
#define SERIESRESISTOR 10000    
 
int amostra[NUMAMOSTRAS];
int i;
int valor = 0; // Variavel onde e guardado o valor lido do LDR
int led_pin = 10; // Pino onde vamos ligar o LED
int analogin = 5; // Pino onde vamos ligar a LDR

void setup(void) {
  pinMode (led_pin,OUTPUT);
  Serial.begin(9600);
  analogReference(EXTERNAL);
}
 
void loop(void) {

  //======================= SENSOR LDR =======================================
  {

    valor = analogRead(analogin); // O valor que irá ser lido na porta analogica numero 5 irá ser guardado na variavel “valor”
    Serial.print("O valor Da LDR e igual a: "); // Mostrar na consola a frase “O valor Da LDR e igual a: ”
    Serial.println(valor); // Mostras no Serial Monitor o valor da variavel “valor”
    delay(25); // Faz uma pequena pausa de 25 Mili Segundos

    if ( valor < 50) { // Se a variavel valor for menor que 300
    digitalWrite(led_pin, HIGH); // acende o led da porta 10
    } else { digitalWrite(led_pin, LOW); } // se for maior mantem o led da porta 10 apagado
          }
  //========================== Sensor de Temperatura===================================
 
   float media;
 
    for (i=0; i< NUMAMOSTRAS; i++) {
       amostra[i] = analogRead(PINOTERMISTOR);
         delay(10);
        }
 
    media = 0;
      for (i=0; i< NUMAMOSTRAS; i++) {
     media += amostra[i];
    }
  media /= NUMAMOSTRAS;

  // Converte o valor da tensão em resistência
  media = 1023 / media - 1;
  media = SERIESRESISTOR / media;
  
  //Faz o cálculo da temperatura
  float temperatura;
  temperatura = media / TERMISTORNOMINAL;     // (R/Ro)
  temperatura = log(temperatura); // ln(R/Ro)
  temperatura /= BCOEFFICIENT;                   // 1/B * ln(R/Ro)
  temperatura += 1.0 / (TEMPERATURENOMINAL + 273.15); // + (1/To)
  temperatura = 1.0 / temperatura;                 // Inverte o valor
  temperatura -= 273.15;                         // Converte para Celsius
 
  Serial.print("Temperatura no Sensor eh: ");
  Serial.print(temperatura);
  Serial.println(" *C");
 
  delay(1000);
}
