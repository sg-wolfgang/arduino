#include <Servo.h>
#include <IRremote.h>
#include <IRremoteInt.h>

//Projeto ligar servo com controle

int cont_rm = 11;
IRrecv irrecv(cont_rm);
decode_results results;
Servo servo1;
int pos = 0;

void setup(){
    servo1.attach(10);
    Serial.begin(9600); // declarando o serial monitor para que possamos enxergar o valor de cada botao no controle remoto
    irrecv.enableIRIn(); // iniciando o receptor de infravermelho
}

void loop () {


if (irrecv.decode(&results)) {

Serial.println(results.value, DEC);// Aqui voce mostra no serial monitor em decimal o resultado de cada botao digitado

irrecv.resume(); // Aqui é para receber o proximo valor

if (results.value == 0x1FE50AF)
{ 
  for(pos = 0; pos < 180; pos += 1)  // goes from 0 degrees to 180 degrees 
  {                                  // in steps of 1 degree 
    servo1.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  } }
else if (results.value == 0x1FE7887)
 { for(pos = 180; pos>=1; pos-=1)     // goes from 180 degrees to 0 degrees 
  {                                
    servo1.write(pos);              // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  } 
} 
}


}//loop roda tudo sem parar


