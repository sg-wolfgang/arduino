// Projeto Monitoramento de Temperatura e Humidade com Arduino
//Developed by: Samuel Gonçalves Pereira - UEG Itaberai

#include "DHT.h"

#define DHTPIN A2//Pino onde conectei o meu sensor DHT
#define DHTTYPE DHT11//Modelo do sensor 
DHT dht(DHTPIN, DHTTYPE);

// Aqui inicia-se um codigo para ligaço de um LCD
// LCD RS=3, EN=4, DS4=5, DS5=6, DS6=7, DS7=8
#include <LiquidCrystal.h>
LiquidCrystal lcd( 3, 4, 5, 6, 7, 8);

#include <EtherCard.h>

// Definiçao dos parametros de rede
static byte mymac[] = { 0x74,0x69,0x69,0x2D,0x30,0x31 };
static byte myip[] = { 192,168,1,199 };
static byte gwip[] = { 192,168,1,1 };
byte Ethernet::buffer[500];
BufferFiller bfill;

int t=0;
int h=0;
int Timer= 0;
int Animation = 0;

// Definiçao da funçao setup, onde inicia-se os principais serviços
void setup () {
  if (ether.begin(sizeof Ethernet::buffer, mymac, 10) == 0)
    Serial.println( "Failed to access Ethernet controller");
  ether.staticSetup(myip, gwip);
  lcd.begin(16,2);
  dht.begin();
}

//Aqui se le os valores do DHT
static void ReadDHT11()
{
  h = dht.readHumidity();
  t = dht.readTemperature();
}

// Codigo da pagina de internet
static word homePage() {
  //ReadDHT11();

  bfill = ether.tcpOffset();
  bfill.emit_p(PSTR(
  "HTTP/1.0 200 OK\r\n"
    "Content-Type: text/html\r\n"
    "Pragma: no-cache\r\n"
    "\r\n"
    "<meta http-equiv='refresh' content='15'/>"
    "<title>Monitoramento</title>" 
    "<h1>Monitoramento de temperatura e humidade.<br/><br/></h1>"
    "<h2>Temperatura: $D C <br>Humidade: $D %</h2>"
    "<h4> Developed by: Samuel Gonçalves Pereira</h4>"),
  t, h);
  return bfill.position();
}
void loop () {

  Timer  = Timer+1;
  if(Timer==1)
  {
    ReadDHT11();
    // set the cursor to (0,0):
    lcd.setCursor(0, 0);
    // print from 0 to 9:

    lcd.print("Temperatura: ");
    lcd.print(t);
    lcd.print("C");
    // set the cursor to (16,1):
    lcd.setCursor(0,1);
    lcd.print("Humidade: ");
    lcd.print(h);
    lcd.print("%");

    if(Animation==0)
    {
      lcd.print("  *");
    }
    if(Animation==1)
    {
      lcd.print("  +");
    }
    Animation = Animation + 1;
    if(Animation==2)
    {
      Animation=0;
    }
  }
  if(Timer==1100)
  {
    Timer=0;
  }

  word len = ether.packetReceive();
  word pos = ether.packetLoop(len);

  if (pos)  // check if valid tcp data is received
  {
    ReadDHT11();
    ether.httpServerReply(homePage()); // send web page data
  }
}

