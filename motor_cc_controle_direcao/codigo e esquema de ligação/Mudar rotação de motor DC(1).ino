/***************************************\ 
**          HOLANDA ENTERPRISE         ** 
*                                       * 
**              DOOR LOCK              ** 
\***************************************/  
int D0=10; //Porta que comando o sentido horario 
int D1=6; //Porta que comanda o sentido anti-horario 
int Botao = 2; //Porta onde esta o primeiro push button
int Botao2 = 3; //Porta onde esta o segundo push button
int EstadoBotao = 0; //Define o estado do Botao
int EstadoBotao2 = 0; //Define o estado do Botao2
void setup(){ 
pinMode(D0,OUTPUT); //Define D0 como porta de saida
pinMode(D1,OUTPUT); //Define D1 como porta de saida
pinMode(Botao, INPUT); //Define o Botao como porta de entrada
pinMode(Botao2, INPUT); //Define o Botao2 como porta de entrada 
} 
void loop(){ 
  EstadoBotao = digitalRead(Botao);
  EstadoBotao2 = digitalRead(Botao2);
  if (EstadoBotao == HIGH){ //Basicamente, se o Botao mudar de estado
digitalWrite(D0,HIGH); //faz com que a porta D0 seja ativada, ou seja, o motor seja ativado.
  }
  else{ //Caso o Botao esteja LOW, ou seja, sem ta apertado
digitalWrite(D0,LOW); //a porta D0 fica LOW, ou seja, o motor fica parado.
  }
if (EstadoBotao2 == HIGH){ 
digitalWrite(D1,HIGH); 
}
else{
digitalWrite(D1,LOW);  
} 
}
