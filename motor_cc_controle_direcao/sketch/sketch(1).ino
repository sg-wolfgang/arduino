/*
Projeto Arduino motor cc com ponte H dupla. Gira nos dois
sentidos e possui controle de potência.
Por Jota
----------------------------------------
--=<| www.ComoFazerAsCoisas.com.br |>=--
----------------------------------------
*/

#define chaveMudaSentRot 2 //Chave que muda o sentido da rotação do motor.
#define motorCCEnt1 3      //INPUT 1 do L293D.
#define motorCCEnt2 4      //INPUT 2 do L293D.
#define controleL293D 9    //Pino de ativação e controle do L293D.
#define controleVelocPot 0 //Pino analógico para controle de potência/velocidade do motor.
int potVelPotencia = 0;    //Variável para armazenar o valor lido no potenciômetro.

void setup() {
  //O pino da chave seletora de rotação definido como de entrada.
  pinMode(chaveMudaSentRot,INPUT);
 
  //Pinos do motor e de ativação do L293D definidos como de saída.
  pinMode(motorCCEnt1,OUTPUT);
  pinMode(motorCCEnt2,OUTPUT);
  pinMode(controleL293D,OUTPUT);
}

void loop() {
  //Lendo valor do potenciometro para aplicar a potência/velocidade ao motor.
  //O valor lido é dividido por 4 para se obter valores entre 0 e 255;
  potVelPotencia = analogRead(controleVelocPot) / 4;
 
  //Transfere o valor lido de velocidade/potência no potenciômetro para o L293D.
  analogWrite(controleL293D,potVelPotencia);
 
  //Controlando o sentido de rotação do motor pela chave liga/desliga e pelos
  //INPUTs do L293d.
  if (digitalRead(chaveMudaSentRot) == HIGH) {
    digitalWrite(motorCCEnt1,LOW);
    digitalWrite(motorCCEnt2,HIGH);
  } else {
    digitalWrite(motorCCEnt1,HIGH);
    digitalWrite(motorCCEnt2,LOW);
  }
} 
