//Projeto Pisca Led

void setup(){
    pinMode(11, OUTPUT);//Aqui esta a declaraçao do pino e sua funçao
}

void loop () {

  digitalWrite(11,HIGH); //aqui ele manda o valor maximo (energia =1)
  delay(300);//para aguardar 1 segundo
  digitalWrite(11,LOW);//aqui ele tira a energia (energia=0)
  delay(1000);  
}//loop roda tudo sem parar
