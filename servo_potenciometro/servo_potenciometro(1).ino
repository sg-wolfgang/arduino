/*madrake 1.0 - dia 18/07/2014
projeto baseado em aula de Neri, com modificaçao de Samuel Gonçalves Pereira
Trabalhando com servomotor e potenciometro*/

#include <Servo.h> 
 
Servo myservo;
int pinPot=0;
int valorPot=0;
int angulo=0;
int pos = 0; //informando a posiçao do servo
 
void setup() 
{ 
  Serial.begin(9600);
  myservo.attach(9); //pino do servo
} 
 
 
void loop() 
{ 
  valorPot=analogRead(pinPot);//frase onde o potenciometro e mostrado para ser lido
  angulo=map(valorPot,0,1023,0,180);//usado para servo ler o potenciometro
  myservo.write(angulo);
  Serial.println(angulo);
  delay(20);
} 
