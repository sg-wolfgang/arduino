//Programa : Modulo ethernet ENC28J60 com LM35 e potenciometro
//Autor : FILIPEFLOP
//Modificações: Samuel Gonçalves Pereira
#include "DHT.h"
#define DHTPIN A1 // pino que estamos conectado
#define DHTTYPE DHT11 // DHT 11
#include "etherShield.h"
#include "ETHER_28J60.h"

DHT dht(DHTPIN, DHTTYPE);
 
//Define o MAC da placa de rede. Nao eh necessario alterar
static uint8_t mac[6] = {0x54, 0x55, 0x58, 0x10, 0x00, 0x24};
//Define o endereco IP da sua placa
static uint8_t ip[4] = {192, 168, 1, 199};
 
static uint16_t port = 80; 
 
ETHER_28J60 e;
 
int pin = A1; // Pino analogico para ligacao do LM35
int tempc = 0; // Variavel que armazena a temperatura em Celsius
// Variáveis para temperatura máxima e mínima
int maxtemp = -100,mintemp = 100; 
int i;
 
void setup()
{ 
  Serial.begin(9600);
  dht.begin();
  //Inicializa a placa com as configuracoes fornecidas
  e.setup(mac, ip, port);
}
 
void loop()
{
  //Calcula o valor da temperatura
  float h = dht.readHumidity();
  float t = dht.readTemperature();
 
  //Armazena a temperatura máxima na variavel maxtemp
  if(tempc > maxtemp) {maxtemp = tempc;} 
  //Armazena a temperatura máxima na variavel mintemp
  if(tempc < mintemp) {mintemp = tempc;} 
 
  if (e.serviceRequest())
  {
    e.print("<H1>Monitoramento de Temperatura com ENC28J60</H1><br/>");
    e.print("<br/><br/>");
    e.print("Valor temperatura : ");
    e.print(tempc);
    e.print("&deg<br/><br/>");
    e.print("Temperatura minima : ");
    e.print(mintemp);
    e.print("&deg<br/>");
    e.print("Temperatura maxima : ");
    e.print(maxtemp);
     e.print("&deg<b/>");
    e.print("<br/><br/><br/>Developed by: Samuel G. Pereira");
   
    e.respond();
  }
  delay(100);
}
